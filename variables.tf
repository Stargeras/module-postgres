variable "config_path" {
  default = ""
}

variable "kubernetes_auth_host" {
  description = "The hostname (in form of URI) of the Kubernetes API"
  type        = string
  default     = ""
}
variable "kubernetes_auth_cluster_ca_certificate" {
  description = "PEM-encoded root certificates bundle for TLS authentication"
  type        = string
  default     = ""
}
variable "kubernetes_auth_client_certificate" {
  default     = ""
  description = "PEM-encoded client certificate for TLS authentication"
  type        = string
}
variable "kubernetes_auth_client_key" {
  default     = ""
  description = "PEM-encoded client certificate key for TLS authentication"
  type        = string
}
variable "kubernetes_auth_token" {
  default     = ""
  description = "Service account token"
  type        = string
}
variable "registry_server" {
  default     = "registry.gitlab.com"
  description = "Endpoint of registry containing helmchart"
  type        = string
}
variable "registry_username" {
  description = "Username for authentication to registry_server. Not needed if registry is public."
  type        = string
  default     = ""
}
variable "registry_password" {
  description = "Password for authentication to registry_server. Not needed if registry is public."
  type        = string
  default     = ""
}

variable "ingress_domain" {
  default     = "example.com"
  description = "Domain name for ingress endpoints"
  type        = string
}
variable "ingress_class_name" {
  default     = "nginx"
  description = "Class name for ingress resources"
  type        = string
}
variable "pgadmin_enabled" {
  default     = true
  description = "Enable deployment of pgadmin"
  type        = bool
}
variable "superset_enabled" {
  default     = true
  description = "Enable deployment of superset"
  type        = bool
}
