resource "helm_release" "postgres" {
  name             = "postgres"
  namespace        = "postgres"
  create_namespace = "true"
  repository       = "oci://${var.registry_server}"
  chart            = "${var.chart_path}"
  version          = "${var.chart_version}"
  wait             = "false"
  timeout          = "600"

  values = [
    templatefile("${path.module}/values-tmpl.yaml", {
      ingress_domain     = var.ingress_domain
      ingress_class_name = var.ingress_class_name
      pgadmin_enabled    = var.pgadmin_enabled
      superset_enabled   = var.superset_enabled
    })
  ]
}
