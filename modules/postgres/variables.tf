variable "ingress_domain" {}
variable "ingress_class_name" {}
variable "pgadmin_enabled" {}
variable "superset_enabled" {}
variable "registry_server" {}
variable "chart_version" {}
variable "chart_path" {}
