<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_postgres"></a> [postgres](#module\_postgres) | ./modules/postgres | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | Class name for ingress resources | `string` | `"nginx"` | no |
| <a name="input_ingress_domain"></a> [ingress\_domain](#input\_ingress\_domain) | Domain name for ingress endpoints | `string` | `"example.com"` | no |
| <a name="input_kubernetes_auth_client_certificate"></a> [kubernetes\_auth\_client\_certificate](#input\_kubernetes\_auth\_client\_certificate) | PEM-encoded client certificate for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_client_key"></a> [kubernetes\_auth\_client\_key](#input\_kubernetes\_auth\_client\_key) | PEM-encoded client certificate key for TLS authentication | `string` | `""` | no |
| <a name="input_kubernetes_auth_cluster_ca_certificate"></a> [kubernetes\_auth\_cluster\_ca\_certificate](#input\_kubernetes\_auth\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication | `string` | n/a | yes |
| <a name="input_kubernetes_auth_host"></a> [kubernetes\_auth\_host](#input\_kubernetes\_auth\_host) | The hostname (in form of URI) of the Kubernetes API | `string` | n/a | yes |
| <a name="input_kubernetes_auth_token"></a> [kubernetes\_auth\_token](#input\_kubernetes\_auth\_token) | Service account token | `string` | `""` | no |
| <a name="input_pgadmin_enabled"></a> [pgadmin\_enabled](#input\_pgadmin\_enabled) | Enable deployment of pgadmin | `bool` | `true` | no |
| <a name="input_registry_password"></a> [registry\_password](#input\_registry\_password) | Password for authentication to registry\_server. Not needed if registry is public. | `string` | `""` | no |
| <a name="input_registry_server"></a> [registry\_server](#input\_registry\_server) | Endpoint of registry containing helmchart | `string` | `"registry.gitlab.com"` | no |
| <a name="input_registry_username"></a> [registry\_username](#input\_registry\_username) | Username for authentication to registry\_server. Not needed if registry is public. | `string` | `""` | no |
| <a name="input_superset_enabled"></a> [superset\_enabled](#input\_superset\_enabled) | Enable deployment of superset | `bool` | `true` | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->