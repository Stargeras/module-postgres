provider "helm" {
  kubernetes {
    config_path            = var.config_path
    host                   = var.kubernetes_auth_host
    cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
    client_certificate     = var.kubernetes_auth_client_certificate
    client_key             = var.kubernetes_auth_client_key
    token                  = var.kubernetes_auth_token
  }
  /*
  registry {
    url      = "oci://${var.registry_server}"
    username = var.registry_username
    password = var.registry_password
  }
  */
}

provider "kubernetes" {
  config_path            = var.config_path
  host                   = var.kubernetes_auth_host
  cluster_ca_certificate = var.kubernetes_auth_cluster_ca_certificate
  client_certificate     = var.kubernetes_auth_client_certificate
  client_key             = var.kubernetes_auth_client_key
  token                  = var.kubernetes_auth_token
}
