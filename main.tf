locals {
  postgres_chart_version = "1.2.1"
  postgres_chart_path    = "stargeras/helm-postgres/postgres"
}

module "postgres" {
  source             = "./modules/postgres"
  ingress_domain     = var.ingress_domain
  ingress_class_name = var.ingress_class_name
  pgadmin_enabled    = var.pgadmin_enabled
  superset_enabled   = var.superset_enabled
  registry_server    = var.registry_server
  chart_version      = local.postgres_chart_version
  chart_path         = local.postgres_chart_path
}
